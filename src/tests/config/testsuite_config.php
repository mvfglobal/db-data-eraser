<?php

 return [
     'connection' => [
         'host' => getenv('MYSQL_TESTING_HOST'),
         'database' =>getenv('MYSQL_DATABASE'),
         'username' => getenv('MYSQL_USER'),
         'password' => getenv('MYSQL_PASSWORD'),
     ],
     'valid table config' => [
         'connection' => [
             'host' => getenv('MYSQL_TESTING_HOST'),
             'database' =>getenv('MYSQL_DATABASE'),
             'username' => getenv('MYSQL_USER'),
             'password' => getenv('MYSQL_PASSWORD'),
         ],
         'tables' => [
             [
                 'name' => 'test_table_1',
                 'datetime_column' => 'created_at',
                 'max_age' => '-30 days',
                 'date_format' => 'Y-m-d H:i:s',
                 'limit' => 1000,
                 'order_by' => 'created_at',
             ],
             [
                 'name' => 'test_table_2',
                 'datetime_column' => 'created_at',
                 'max_age' => '-30 days',
                 'date_format' => 'Y-m-d H:i:s',
                 'limit' => 1000,
                 'order_by' => 'created_at',
             ],
         ],
     ],
     'invalid empty table config' => [
         'connection' => [
             'host' => getenv('MYSQL_TESTING_HOST'),
             'database' =>getenv('MYSQL_DATABASE'),
             'username' => getenv('MYSQL_USER'),
             'password' => getenv('MYSQL_PASSWORD'),
         ],
         'tables' => [],
     ],
     'valid table config using unix timestamp' => [
         'connection' => [
             'host' => getenv('MYSQL_TESTING_HOST'),
             'database' =>getenv('MYSQL_DATABASE'),
             'username' => getenv('MYSQL_USER'),
             'password' => getenv('MYSQL_PASSWORD'),
         ],
         'tables' => [
             [
                 'name' => 'test_table_1',
                 'datetime_column' => 'created_at',
                 'max_age' => '-30 days',
                 'date_format' => 'U',
                 'limit' => 1000,
                 'order_by' => 'created_at',
             ],
             [
                 'name' => 'test_table_2',
                 'datetime_column' => 'created_at',
                 'max_age' => '-30 days',
                 'date_format' => 'U',
                 'limit' => 1000,
                 'order_by' => 'created_at',
             ],
         ],
     ],
 ];