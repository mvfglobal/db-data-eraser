<?php

use Carbon\Carbon;
use Illuminate\Database\Capsule\Manager as Capsule;
use MVF\DataEraser\DataEraser;
use PHPUnit\Framework\TestCase;

class DataEraserTest extends TestCase
{
    private $config;
    private $now;
    private $database;

    private const TABLE_NAME_1 = 'test_table_1';
    private const TABLE_NAME_2 = 'test_table_2';

    public function setUp(): void
    {
        parent::setUp();

        $this->config = require dirname(dirname(__FILE__)) . '/tests/config/testsuite_config.php';
        $this->now = Carbon::now();
        $this->database = $this->configureDatabase();
        $this->dropTables();
    }

    public function tearDown(): void
    {
        $this->dropTables();

        parent::tearDown();
    }

    public function testHandlesATimestampsDatetimeColumnType()
    {
        $this->buildTablesWithTimestamps();
        $this->database->table(self::TABLE_NAME_1)->insert([
            ['value' => 'table 1 test data', 'created_at' => $this->now, 'updated_at' => $this->now],
            ['value' => 'table 1 older than 30 days test data', 'created_at' => '2019-07-29 15:11:06', 'updated_at' => '2019-07-29 15:11:06'],
        ]);
        $this->database->table(self::TABLE_NAME_2)->insert([
            ['value' => 'table 2 test data', 'created_at' => $this->now, 'updated_at' => $this->now],
            ['value' => 'table 2 older than 30 days test data', 'created_at' => '2019-07-29 15:11:06', 'updated_at' => '2019-07-29 15:11:06'],
        ]);

        $dataEraser = new DataEraser($this->config['valid table config']);
        $dataEraser->run();
        $result = [];

        foreach ($this->config['valid table config']['tables'] as $tableConfig) {
            $result[] = $this->database->table($tableConfig['name'])
                ->get()
                ->map(function ($item) {
                return (array) $item;
            })
                ->all();
        }

        $expectedResult = [
            [
                [
                    'id' => 1,
                    'value' => 'table 1 test data',
                    'created_at' => $this->now,
                    'updated_at' => $this->now,
                ],
            ],
            [
                [
                    'id' => 1,
                    'value' => 'table 2 test data',
                    'created_at' => $this->now,
                    'updated_at' => $this->now,
                ],
            ],
        ];

        $this->assertEquals($expectedResult, $result);
    }

    public function testHandlesAUnixTimestampDatetimeColumnType()
    {
        $schemaBuilder = $this->database->getSchemaBuilder();
        $schemaBuilder->create(self::TABLE_NAME_1, function ($table) {
            $table->increments('id');
            $table->string('value');
            $table->integer('created_at');
        });
        $schemaBuilder->create(self::TABLE_NAME_2, function ($table) {
            $table->increments('id');
            $table->string('value');
            $table->integer('created_at');
        });

        $unixTimeNow = Carbon::now()->timestamp;
        $oldUnixTimestamp = 1592191300;

        $this->database->table(self::TABLE_NAME_1)->insert([
            ['value' => 'table 1 test data', 'created_at' => $unixTimeNow],
            ['value' => 'table 1 older than 30 days test data', 'created_at' => $oldUnixTimestamp],
        ]);
        $this->database->table(self::TABLE_NAME_2)->insert([
            ['value' => 'table 2 older than 30 days test data', 'created_at' => $oldUnixTimestamp],
        ]);

        $dataEraser = new DataEraser($this->config['valid table config using unix timestamp']);
        $dataEraser->run();
        $result = [];

        foreach ($this->config['valid table config using unix timestamp']['tables'] as $tableConfig) {
            $result[] = $this->database->table($tableConfig['name'])
                ->get()
                ->map(function ($item) {
                    return (array) $item;
                })
                ->all();
        }

        $expectedResult = [
            [
                [
                    'id' => 1,
                    'value' => 'table 1 test data',
                    'created_at' => $unixTimeNow,
                ],
            ],
            [],
        ];

        $this->assertEquals($expectedResult, $result);
    }

    public function testHandlesADatetimeColumnType()
    {
        $schemaBuilder = $this->database->getSchemaBuilder();
        $schemaBuilder->create(self::TABLE_NAME_1, function ($table) {
            $table->increments('id');
            $table->string('value');
            $table->dateTime('created_at');
        });
        $schemaBuilder->create(self::TABLE_NAME_2, function ($table) {
            $table->increments('id');
            $table->string('value');
            $table->dateTime('created_at');
        });

        $this->database->table(self::TABLE_NAME_1)->insert([
            ['value' => 'table 1 test data', 'created_at' => $this->now],
            ['value' => 'table 1 older than 30 days test data', 'created_at' => '2019-07-29 15:11:06'],
        ]);
        $this->database->table(self::TABLE_NAME_2)->insert([
            ['value' => 'table 2 test data', 'created_at' => $this->now],
            ['value' => 'table 2 older than 30 days test data', 'created_at' => '2019-07-29 15:11:06'],
        ]);

        $dataEraser = new DataEraser($this->config['valid table config']);
        $dataEraser->run();
        $result = [];

        foreach ($this->config['valid table config']['tables'] as $tableConfig) {
            $result[] = $this->database->table($tableConfig['name'])
                ->get()
                ->map(function ($item) {
                    return (array) $item;
                })
                ->all();
        }

        $expectedResult = [
            [
                [
                    'id' => 1,
                    'value' => 'table 1 test data',
                    'created_at' => $this->now,
                ],
            ],
            [
                [
                    'id' => 1,
                    'value' => 'table 2 test data',
                    'created_at' => $this->now,
                ],
            ],
        ];

        $this->assertEquals($expectedResult, $result);
    }

    public function testReturnsFalseIfTablesConfigIsEmpty()
    {
        $invalidEmptyTableConfig = $this->config['invalid empty table config'];
        $dataEraser = new DataEraser($invalidEmptyTableConfig);

        $this->assertEquals(false, $dataEraser->run());
    }

    public function testRunsSuccessfullyIfTimestampIsNull()
    {
        $this->buildTablesWithTimestamps();

        $this->database->table(self::TABLE_NAME_1)->insert([
            ['value' => 'table 1 test data', 'created_at' => null, 'updated_at' => null],
            ['value' => 'table 1 older than 30 days test data', 'created_at' => '2019-07-29 15:11:06', 'updated_at' => '2019-07-29 15:11:06'],
        ]);
        $this->database->table(self::TABLE_NAME_2)->insert([
            ['value' => 'table 2 test data', 'created_at' => null, 'updated_at' => null],
        ]);

        $dataEraser = new DataEraser($this->config['valid table config']);

        $this->assertEquals(true, $dataEraser->run());
    }

    public function testGetsResultArray()
    {
        $this->buildTablesWithTimestamps();
        $this->database->table(self::TABLE_NAME_1)->insert([
            ['value' => 'table 1 test data', 'created_at' => $this->now, 'updated_at' => $this->now],
            ['value' => 'table 1 older than 30 days test data', 'created_at' => '2019-07-29 15:11:06', 'updated_at' => '2019-07-29 15:11:06'],
        ]);
        $this->database->table(self::TABLE_NAME_2)->insert([
            ['value' => 'table 2 test data', 'created_at' => $this->now, 'updated_at' => $this->now],
            ['value' => 'table 2 older than 30 days test data', 'created_at' => '2019-07-29 15:11:06', 'updated_at' => '2019-07-29 15:11:06'],
        ]);

        $dataEraser = new DataEraser($this->config['valid table config']);
        $dataEraser->run();

        $expectedResult = [
            "test_table_1" =>
            [
                'success' => true
            ],
            'test_table_2' =>
            [
                'success' => true
            ],
        ];

        $this->assertEquals($expectedResult, $dataEraser->getResultsArray());
    }

    public function testGetsNumberOfRowsWithNullTimestamps()
    {
        $this->buildTablesWithTimestamps();

        $this->database->table(self::TABLE_NAME_1)->insert([
            ['value' => 'table 1 null timestamps test data', 'created_at' => NULL, 'updated_at' => NULL],
            ['value' => 'table 1 older than 30 days test data', 'created_at' => '2019-07-29 15:11:06', 'updated_at' => '2019-07-29 15:11:06'],
        ]);
        $this->database->table(self::TABLE_NAME_2)->insert([
            ['value' => 'table 2 null timestamps test data', 'created_at' => null, 'updated_at' => null],
            ['value' => 'table 2 null timestamps test data', 'created_at' => null, 'updated_at' => null],
            ['value' => 'table 2 null timestamps test data', 'created_at' => null, 'updated_at' => null],
            ['value' => 'table 2 null timestamps test data', 'created_at' => '2019-07-29 15:12:06', 'updated_at' => null],
        ]);

        $dataEraser = new DataEraser($this->config['valid table config']);
        $dataEraser->run();

        $expectedResult = [
            "test_table_1" =>
                [
                    'number_of_rows_with_null_timestamps' => 1
                ],
            'test_table_2' =>
                [
                    'number_of_rows_with_null_timestamps' => 3
                ],
        ];

        $this->assertEquals($expectedResult, $dataEraser->getRowsWithNullTimestampArray());
    }

    private function configureDatabase(): \Illuminate\Database\Connection
    {
        $DB = new Capsule;
        $DB->addConnection([
            'driver' => 'mysql',
            'host' => $this->config['connection']['host'],
            'database' => $this->config['connection']['database'],
            'username' => $this->config['connection']['username'],
            'password' => $this->config['connection']['password'],
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
        ], 'testing-db');
        $DB->bootEloquent();

        return $this->database = $DB->getConnection('testing-db');
    }

    private function buildTablesWithTimestamps()
    {
        $schemaBuilder = $this->database->getSchemaBuilder();
        $schemaBuilder->create(self::TABLE_NAME_1, function ($table) {
            $table->increments('id');
            $table->string('value');
            $table->timestamps();
        });
        $schemaBuilder->create(self::TABLE_NAME_2, function ($table) {
            $table->increments('id');
            $table->string('value');
            $table->timestamps();
        });
    }

    private function dropTables()
    {
        $schemaBuilder = $this->database->getSchemaBuilder();
        $schemaBuilder->dropIfExists(self::TABLE_NAME_1);
        $schemaBuilder->dropIfExists(self::TABLE_NAME_2);
    }
}
