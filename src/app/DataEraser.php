<?php

namespace MVF\DataEraser;

use Illuminate\Database\Capsule\Manager as Capsule;
use DateTime;

class DataEraser
{
    private $database;
    private $config;
    private $results = [];
    private $rowsWithNullTimestamps = [];

    public function __construct($config)
    {
        $capsule = new Capsule;
        $capsule->addConnection([
            'driver' => 'mysql',
            'host' => $config['connection']['host'],
            'database' => $config['connection']['database'],
            'username' => $config['connection']['username'],
            'password' => $config['connection']['password'],
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
        ]);
        $capsule->bootEloquent();
        $this->database = $capsule->getConnection();
        $this->config = $config;
    }

    public function run(): bool
    {
        if (!$this->config['tables']) {
            return false;
        }

        foreach ($this->config['tables'] as $tableConfig) {
            $this->processTruncateTable($tableConfig);
        }

        return true;
    }

    public function getResultsArray(): array
    {
        return $this->results;
    }
    
    public function getRowsWithNullTimestampArray(): array
    {
        return $this->rowsWithNullTimestamps;
    }

    private function processTruncateTable(array $tableConfig): bool
    {
        $dateFormat = $tableConfig['date_format'] ?? 'Y-m-d H:i:s';
        $limit = $tableConfig['limit'] ?? 1000;
        $orderBy = $tableConfig['order_by'] ?? $tableConfig['datetime_column'];
        $oldestDateTime = $this->getOldestDateTime($tableConfig['max_age']);

        try {
            $this->database->table($tableConfig['name'])
                ->where($tableConfig['datetime_column'], '<', $oldestDateTime->format($dateFormat))
                ->orderBy($orderBy)
                ->limit($limit)
                ->delete();

            $this->results[$tableConfig['name']]['success'] = true;

            $rowsWillNullTimeStamps = $this->getRowsWithNullTimestamp($tableConfig['name'], $tableConfig['datetime_column']);
            if ($rowsWillNullTimeStamps !== 0) {
                $this->rowsWithNullTimestamps[$tableConfig['name']]['number_of_rows_with_null_timestamps'] = $rowsWillNullTimeStamps;
            }

            return true;
        } catch (\Exception $e) {
            $this->results[$tableConfig['name']]['success'] = false;

            return false;
        }
    }

    private function getRowsWithNullTimestamp(string $tableName, string $dateTimeColumn): int
    {
        return $this->database->table($tableName)->whereNull($dateTimeColumn)->count();
    }

    private function getOldestDateTime(string $maxAge): DateTime
    {
        $dateTime = new DateTime();
        $dateTime->modify($maxAge);

        return $dateTime;
    }
}